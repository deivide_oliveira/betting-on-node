var prompt = require('sync-prompt').prompt;
var colors = require('colors');

playerCash = 100;
 
var bet = function() {
    computerChoice = Math.ceil(Math.random()*11);
    playerBet = prompt("What is your bet, between $5 and $10? ".yellow);
    while (playerBet < 5 || playerBet > 10) {
        console.log("Invalid bet. Please try again \n".red);
        playerBet = prompt("What is your bet, between $5 and $10? ".yellow);    
    }        
    playerChoice = prompt("Choose a number between 1 and 10: ".yellow);
    while (playerChoice < 1 || playerChoice > 10) {
        console.log("Invalid number. Please try again \n".red);
        playerChoice = prompt("Choose a number between 1 and 10: ".yellow);
    }
}
 
var compareResults = function(player1Choice, player2Choice) {
    if (player1Choice === player2Choice) {
        var win = playerBet * 2;
        playerCash = playerCash + win;
        console.log("Computer choice was: ".green + player1Choice + 
            ". Your choice was: ".green + player2Choice + 
            ". Nice play! You won $ ".green + win + 
            ". Your cash is now $ ".green + playerCash + "\n".green);
    } else if (player1Choice === player2Choice -1 || player1Choice === player2Choice + 1) {
        console.log("Computer choice was: ".yellow + player1Choice + 
            ". Your choice was: ".yellow + player2Choice + 
            ". Almost! You will keep your bet. Your cash is now $ ".yellow + playerCash + "\n".yellow);
    } else {
        var lost = playerBet;
        playerCash = playerCash - playerBet;
        console.log("Computer choice was: ".red + player1Choice + 
            ". Your choice was: ".red + player2Choice + 
            ". You have lost your bet! Your cash is now $ ".red + playerCash + "\n".red);
    }
}
 
var match = function() {
    while (playerCash > 0 && playerCash < 140) {
      bet();
      compareResults(computerChoice, playerChoice); 
    }
    if (playerCash >= 140) {
        console.log("Congratulations!! You are great at this game!".bold.rainbow)
    } else {
        console.log("Game Over! You have lost all your money".bold.red);
    }
}
 
match();